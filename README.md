# forward-port
A Traefik Plugin forward remote port to backend


## Configuration

### Static configuration
```yaml
experimental:
  plugins:
    forward-port:
      moduleName: github.com/unnoo/forward-port
```

### Dynamic configuration
```yaml
http:
  routers:
    router:
      rule: host(`test.api.cn`)
      service: svc
      entryPoints:
        - web
      middlewares:
        - "forward-port"

  services:
    svc:
      loadBalancer:
        servers:
          - url: "http://127.0.0.1:8000"

  middlewares:
    forward-port:
      plugin:
        forward-port:
          headers:
            X-Real-Port: 0000
```